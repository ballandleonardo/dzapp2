/*
** API Service
*/
app.factory('apiService', function($http){
    
    var apiService = {
        users: [], //iniciando o array
    };
    
   apiService.getUsers = function(){
        $http.get('http://pfobeta15.herokuapp.com/api/v1/users').success(function(res){
            apiService.users = res;
        }).error(function(err, status, headers){
            console.log("Error : " + err);
        });
    };

    return apiService;
});