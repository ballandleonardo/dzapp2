/*
** TODOLIST Page Controller
*/
app.controller("TodoCtrl", function($scope, $ionicPopup, apiService){

    $scope.tasks = [];
    
    $scope.addTask = function(){
      $ionicPopup.prompt({
          title: "New task",
          template: "Write the task : ",
          inputPlaceholder: "What do you need to do?",
          okText: 'Add task'
      }).then(function(res){
          if (res) $scope.tasks.push({title: res, completed: false});
      })      
    };
    
    $scope.editTask = function(task){
        $scope.data = { response: task.title };
        $ionicPopup.prompt({
            title: "Edit task",
            scope: $scope
        }).then(function(res){
            if (res !== undefined) task.title = $scope.data.response;
            $ionicListDelegate.closeOptionButtons();
        })
    };
});
