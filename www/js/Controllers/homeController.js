/*
** HOME Page Controller
*/
app.controller("HomeCtrl", function($scope, $ionicLoading, $ionicBackdrop, $timeout){
    
    $scope.text = "";
    
    $scope.showLoad = function(){
        $ionicLoading.show({
          template: 'Showing a loading screen...',
            duration: 2000
        });
    };
    
    $scope.backdrop = function() {
        $ionicBackdrop.retain();
        $timeout(function() {
          $ionicBackdrop.release();
        }, 1000);
    };

});