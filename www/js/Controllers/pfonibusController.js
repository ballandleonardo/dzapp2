/*
** PFONIBUS Page Controller
*/
app.controller("UsersCtrl", function($scope, $ionicLoading, apiService){
    
    $scope.show = "false";
    
    // pull to refresh funcção
    $scope.doRefresh = function() {
        apiService.getUsers();
        $scope.data = apiService;
        $scope.$broadcast('scroll.refreshComplete');
    };
    
    // funcção do butão
    $scope.getUsers = function(){
        $ionicLoading.show({
          template: 'Loading users data...'
        });
        apiService.getUsers(); //liga a funcção "getUsers" do serviço "apiService"
        $scope.data = apiService;
        $scope.show = "true";
        $ionicLoading.hide();
    };
    
    // funcção do butão
    $scope.cleanUsers = function(){
        $scope.show = "false";
        apiService.users = []; // reiniciando o array "apiService.users" -> sem conteudo
    };
});