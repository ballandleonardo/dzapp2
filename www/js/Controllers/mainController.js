/*
** MAIN Controller
*/
app.controller("MainCtrl", function($scope, UserService, $ionicModal, $state, $ionicLoading){
    //Get user info
    $scope.user = UserService.getUser();
    
    //Logout modal
    $ionicModal.fromTemplateUrl('/templates/logout.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal
    })
    
    $scope.showLog = function(){
        $scope.modal.show()
    }

    $scope.logCancel = function() {
        $scope.modal.hide()
    };
    
    $scope.logOk = function(){
        $scope.modal.hide();

        $ionicLoading.show({
          template: 'Logging out...'
        });

        // Facebook logout
        facebookConnectPlugin.logout(function(){
          $ionicLoading.hide();
          $state.go('welcome');
        },
        function(fail){
          $ionicLoading.hide();
        });
    };
});