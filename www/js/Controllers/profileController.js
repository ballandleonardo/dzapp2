/*
** PROFILE Controller
*/
app.controller("ProfileCtrl", function($scope, UserService){
    $scope.test = "test";
    
    var userc = Ionic.User.current();
    $scope.name = userc.get('name');
    $scope.email = userc.get('email');
    $scope.bio = userc.get('bio');
});