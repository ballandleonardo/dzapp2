var app = angular.module('starter', ['ionic', 'ionic.service.core', 'ionic.service.push']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }    
    
      // Push system
    var io = Ionic.io();
    var push = new Ionic.Push({
      "onNotification": function(notification) {
        alert('Received push notification!');
      },
      "pluginConfig": {
        "android": {
            "icon": "ic_stat_notif",
            "smallIcon": "ic_stat_notif",
            "iconColor": "#1BD964"
        }
      }
    });
    var user = Ionic.User.current();
    
    if (!user.id) {
      user.id = Ionic.User.anonymousId();
    }
    
    // Just add some dummy data..
    user.set('name', 'Simon');
    user.set('bio', 'This is my little bio');
    user.save();
   
    var callback = function(data) {
      push.addTokenToUser(user);
      user.save();
    };
    push.register(callback);
  });
});

app.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
    
        .state('welcome', {
            url:('/welcome'),
            templateUrl: 'templates/welcome.html',
            controller: 'LoginCtrl'
        })
    
        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'MainCtrl'
        })

        .state('app.home', {
            url: '/home',
            views: {
                'menuContent': {
                    templateUrl: 'templates/home.html',
                    controller: 'HomeCtrl'
                }
            }
        })
    
        .state('app.profile', {
            url: '/profile',
            views: {
                'menuContent': {
                    templateUrl: 'templates/profile.html',
                    controller: 'ProfileCtrl'
                }
            }
        })
    
        .state('app.todo', {
            url: '/todo',
            views: {
                'menuContent': {
                    templateUrl: 'templates/todo.html',
                    controller: 'TodoCtrl'
                }
            }
        })
    
        .state('app.users', {
            url: '/users',
            views: {
                'menuContent': {
                    templateUrl: 'templates/users.html',
                    controller: 'UsersCtrl'
                }
            }
        });
    
    $urlRouterProvider.otherwise("/welcome");
});
