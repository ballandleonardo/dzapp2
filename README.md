# README #

Welcome to the DZAPP by Digitalz. It is a hybrid mobile application sample built with the Ionic Framework to explore new ways of interaction development between an API Web Server and a Mobile Client Application.

### Informations ###

* DIGITALZ is a web digital company, working with clients from Brasil and France.
* DZAPP Version 1.0.0
* [DZAPP2 Homepage](https://bitbucket.org/leodsidz/dzapp2)

### How do I get set up? ###

* Download or Clone the repo

```
#!cmd

clone https://leodsidz@bitbucket.org/leodsidz/dzapp2.git
```

* Install the dev dependencies
```
#!cmd

npm install
```

* Initiate the assets files (scss compiling, js concat, minifying everything and more...)

```
#!cmd

ionic serve
```



### Contributions ###

* Thanks to the Ionic Team to develop this awesome hybrid mobile framework
* The AngularJS Team for developping an amazing product
* Don't hesitate to contact me if you got any questions.

### Who do I talk to? ###

* Leonardo da Silva - BR/FR
* digitalz4k@gmail.com